package com.blackphoenixproductions.apigateway;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain additionalFilterChain(ServerHttpSecurity http) {
        http.authorizeExchange()
                .pathMatchers("/topics/public/**").permitAll()
                .pathMatchers("/posts/public/**").permitAll()
                .pathMatchers("/notifications/public/**").permitAll()
                .pathMatchers("/users/public/**").permitAll()
                .and()
                .authorizeExchange()
                .anyExchange()
                .authenticated()
                .and()
                .csrf().disable()
                .oauth2Login();
        return http.build();
    }

 }
